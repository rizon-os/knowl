#!/bin/bash
##############################################################
## Knowl Bookshelf - INDEX Fiction JSON into ElasticSearch  ##
## Filename: ./bare-metal/index-fiction-json.sh             ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# Run logstash, to create an Elasticsearch index from the MariaDB database
sudo rm -f /etc/logstash/conf.d/*.conf
sudo cp ~/knowl/conf.d/knowl-libgen-fiction.conf /etc/logstash/conf.d
sudo -Hu logstash /usr/share/logstash/bin/logstash --path.settings=/etc/logstash -f /etc/logstash/conf.d/
sudo rm -f /etc/logstash/conf.d/*.conf
