# SCIENCE
# Download a (potentially lagged/outdated) JSON snapshot of the LG Database.
cd ../ingest
rm -f knowl-libgen-science-master-v1_1.*
wget -c https://ipfs.io/ipns/databases.libgen.eth/knowl-libgen-science-master-v1_1.zip
7z x knowl-libgen-science-master-v1_1.zip
rm knowl-libgen-science-master-v1_1.zip
mv knowl-libgen-science-master-v1_1.json knowl-libgen-science.json
curl -XDELETE http://localhost:9200/knowl-libgen-science
sh ../bare-metal/index-science-json.sh
