#!/bin/bash
##############################################################
## Knowl Bookshelf - INGEST Science                         ##
## Filename: ./legacy/2-maria-to-json-science.sh            ##
## https://gitlab.com/lucidhack/knowl                       ##
## https://creativecommons.org/licenses/by/4.0/             ##
## Author(s): lucidhack                                     ##
##############################################################

# PURGE EXISTING FILES
cd /var/tmp
sudo rm -f ~/knowl/ingest/knowl-libgen-science.json
sudo rm -f /var/tmp/knowl-libgen-science*.json

# GENERATE MANY JSON FILES (100K at a time)
counter=0
while [ $counter -le 2500000 ]
do
echo $counter
sudo mysql <<ENDSQL
use libgen;
DROP TABLE IF EXISTS knowl;
SET connect_work_size = 4000000000;
create table knowl (TOC char(255), Description char(255)) engine=CONNECT table_type=JSON file_name='/var/tmp/knowl-libgen-science-$counter.json' option_list='Pretty=1,Jmode=0,Base=1' lrecl=128 as
SELECT updated.ID,
       updated.Title,
       updated.VolumeInfo "Volume",
       updated.Series,
       updated.Author,
       updated.Year,
       updated.Edition,
       updated.Publisher,
       updated.Pages,
       updated.Language,
       updated.Library,
       updated.Issue,
       updated.ISSN,
       updated.ASIN,
       updated.UDC,
       updated.LBC,
       updated.DDC,
       updated.LCC,
       updated.Doi "DOI",
       updated.OpenLibraryID,
       updated.Filesize,
       updated.Extension,
       updated.Coverurl "Cover",
       updated.Tags,
       updated.IdentifierWODash "ISBN",
       hashes.md5 "MD5",
       hashes.crc32 "CRC32",
       hashes.aich "AICH",
       hashes.sha1 "SHA1",
       hashes.tth "TTH",
       hashes.btih "BTIH",
       hashes.sha256 "SHA256",
       description.toc "TOC",
       description.descr "Description",
       topics.topic_descr "Topic"
       FROM libgen.updated LEFT JOIN libgen.description ON libgen.updated.MD5=libgen.description.md5
       LEFT JOIN libgen.hashes ON libgen.updated.md5=libgen.hashes.md5
       LEFT JOIN libgen.topics ON libgen.updated.topic=libgen.topics.topic_id WHERE libgen.topics.lang="en" OR libgen.updated.Topic="" OR libgen.updated.Topic REGEXP "[^-0-9\/]+"
       ORDER BY libgen.updated.id LIMIT $counter,100000;
DROP TABLE IF EXISTS knowl;
ENDSQL
counter=$(( $counter + 100000 ))
done

# COMBINE ALL OF THE 100K JSON FILES
sudo cat knowl-libgen-science*.json >> ~/knowl/ingest/knowl-libgen-science.json
sudo rm -f knowl-libgen-science*.json

# DELETE LINES CONTAINING ONLY "[" or "]",
# AND DELETE LEADING WHITESPACE FROM ALL LINES
cd ~/knowl/ingest
sudo sed -i '/^\[/d;/^\]/d;s/^[ \t]*//' knowl-libgen-science.json

# Next, you should call the index json script
# sudo sh ../bare-metal/index-science-json.sh
